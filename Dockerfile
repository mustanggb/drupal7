FROM alpine:3.11 AS drupal7
ARG ARCHITECTURE=x86_64
ARG S6_OVERLAY_VERSION=3.1.3.0
ARG DRUSH_VERSION=8.4.5
ARG DRUPAL_VERSION=7.94

# Nginx install
RUN apk add --no-cache nginx

# Nginx config
ADD https://gitlab.com/mustanggb/nginx-config/-/archive/master/nginx-config-master.tar.gz /tmp/
#RUN rm -r /etc/nginx/ && \ # @todo: Add
RUN mv /etc/nginx/ /etc/nginx-original/ && \
    mkdir /etc/nginx/ && \
    tar -xzC /etc/nginx/ --strip-components=1 -f /tmp/nginx-config-master.tar.gz && \
    rm /tmp/nginx-config-master.tar.gz
#RUN mv /etc/nginx/ /etc/nginx-original/ && \
#    git clone --depth=1 https://gitlab.com/mustanggb/nginx-config.git /etc/nginx/ && \
#    rm -R /etc/nginx/.git
ADD www.conf /etc/nginx/sites-available/www
RUN ln -s ../sites-available/www /etc/nginx/sites-enabled/www
RUN mkdir /var/cache/nginx/ /var/log/www/

# Packages install
RUN apk add --no-cache nginx-mod-http-upload-progress \
                       nginx-mod-http-headers-more \
                       php7-ctype \
                       php7-curl \
                       php7-dom \
                       php7-fileinfo \
                       php7-fpm \
                       php7-gd \
                       php7-json \
                       php7-mbstring \
                       php7-pecl-imagick \
                       php7-pdo_mysql \
                       php7-phar \
                       php7-session \
                       php7-simplexml \
                       php7-xml \
                       php7-zip \
                       postfix \
                       cyrus-sasl-login \
                       curl \
                       mysql-client \
                       openssl \
                       openssh

# System config
RUN adduser -u 82 -g "www-data" -h "/tmp" -s /bin/false -D -H -S -G www-data www-data

# s6 install
# Based on harningt/docker-base-alpine-s6-overlay
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp/
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-${ARCHITECTURE}.tar.xz /tmp/
RUN tar -xJpC / -f /tmp/s6-overlay-noarch.tar.xz && \
    tar -xJpC / -f /tmp/s6-overlay-${ARCHITECTURE}.tar.xz && \
    rm /tmp/s6-overlay-noarch.tar.xz && \
    rm /tmp/s6-overlay-${ARCHITECTURE}.tar.xz

# s6 config
COPY init/ /etc/s6-overlay/s6-init.d/
COPY services/ /etc/s6-overlay/s6-rc.d/
ENV PATH="${PATH}:/command"

# SSH config
#RUN adduser -u 500 -g "remote" -h "/" -s /bin/sh -D -H remote remote
#RUN adduser -u 500 -g "remote" -s /bin/sh -D remote remote
#RUN ssh-keygen -q -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ''
#RUN sed -i "s/#PermitRootLogin.*/PermitRootLogin no/" /etc/ssh/sshd_config
#RUN sed -i "s/#PasswordAuthentication.*/PasswordAuthentication no/" /etc/ssh/sshd_config
#RUN sed -i "s/#ChallengeResponseAuthentication.*/ChallengeResponseAuthentication no/" /etc/ssh/sshd_config
#RUN sed -i "s/AllowTcpForwarding.*/AllowTcpForwarding yes/" /etc/ssh/sshd_config
#RUN echo "AllowUsers remote" >> /etc/ssh/sshd_config
RUN echo "root:root" | chpasswd
RUN ssh-keygen -A
RUN sed -i "s/#PermitRootLogin.*/PermitRootLogin yes/" /etc/ssh/sshd_config

# PHP config
RUN sed -ri -e 's/^;?user\s?=.*$/user = www-data/' /etc/php7/php-fpm.d/www.conf
RUN sed -ri -e 's/^;?group\s?=.*$/group = www-data/' /etc/php7/php-fpm.d/www.conf
RUN sed -ri -e 's/^;?listen\s?=.*$/listen = \/run\/php7-fpm.sock/' /etc/php7/php-fpm.d/www.conf
RUN sed -ri -e 's/^;?listen\.owner\s?=.*$/listen.owner = nginx/' /etc/php7/php-fpm.d/www.conf
RUN sed -ri -e 's/^;?listen\.group\s?=.*$/listen.group = www-data/' /etc/php7/php-fpm.d/www.conf

# Postfix config
RUN echo "inet_interfaces = loopback-only" > /etc/postfix/main.cf

# Drush install
ADD https://github.com/drush-ops/drush/releases/download/${DRUSH_VERSION}/drush.phar /usr/local/bin/drush-core
ADD drush.sh /usr/local/bin/drush
RUN chmod +rx /usr/local/bin/drush-core /usr/local/bin/drush

# Cron install
ADD cron.sh /etc/periodic/hourly/drupal
RUN chmod +x /etc/periodic/hourly/drupal

# Drupal install
RUN drush-core dl --destination=/srv/ --drupal-project-rename=www drupal-${DRUPAL_VERSION}

# Drupal config
RUN chgrp -R www-data /srv/www/ && \
    mkdir /srv/www/sites/default/files/ && \
    chown www-data:www-data /srv/www/sites/default/files/ && \
    cp /srv/www/sites/default/default.settings.php /srv/www/sites/default/settings.php && \
    chown www-data:www-data /srv/www/sites/default/settings.php

# Give init scripts a 5 minute timeout
ENV S6_CMD_WAIT_FOR_SERVICES_MAXTIME=300000

# Terminate if init scripts fail
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS=2

# Expose ports
EXPOSE 80

# Working directory
WORKDIR /srv/www/

# Run s6
ENTRYPOINT /init
