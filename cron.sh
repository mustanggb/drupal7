#!/bin/sh

# Variables
WWW_PATH=/srv/www/

# Randomise start time to improve collision prevention
sleep $(($RANDOM%30))

# Check cron is not currently running, or hasn't been run recently (last 30 minutes)
if [ $(/usr/local/bin/drush -r $WWW_PATH sqlq "SELECT count(1) FROM semaphore WHERE name = 'cron'") -eq 0 ] && \
   $(php -r "print (time() - unserialize(\"$(/usr/local/bin/drush -r $WWW_PATH sqlq "SELECT value FROM variable WHERE name = 'cron_last'")\") >= 1800) ? 'true' : 'false';"); then
  /usr/local/bin/drush -r $WWW_PATH cron
fi
