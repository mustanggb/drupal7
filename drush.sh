#!/bin/sh

# Run drush as www-data
PATH=$(dirname $0):${PATH}
export DRUSH_FINDER_SCRIPT=$(command -v drush-core)
command=${DRUSH_FINDER_SCRIPT}
for argument in "$@"; do
  command="${command} \"${argument}\""
done
su -s /bin/sh -c "${command}" www-data
